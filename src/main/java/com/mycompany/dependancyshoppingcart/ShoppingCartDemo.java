/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dependancyshoppingcart;

/**
 *
 * @author mymac
 */
public class ShoppingCartDemo {
    public static void main(String[] args) {
        
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PayementService creditService = factory.getPaymentService(PayementServiceType.CREDIT);
         PayementService debitService = factory.getPaymentService(PayementServiceType.DEBIT);
         
         Cart cart = new Cart();
         cart.addProduct(new Product("shirt"),50);
         cart.addProduct(new Product("pants"),60);
         
         cart.setPaymentService (creditService);
         cart.payCart();
         
         cart.setPaymentService(debitService);
         cart.payCart();
       
    }
}
